using LinearAlgebra
using  Distributed


##compute the rouse hamiltonian

function Rouse_Hamiltonian(r,R_e=1)
    N=length(r[:,1])
    H=3/2 * (N - 1) * sum(norm.(r[2:end,:] - r[1:end-1,:]).^2)    
    return H
end

##compute gaussian

function gauss(x,sigma=1,mu=0)
    return 1/sqrt(2*pi*sigma^2)*exp(-(x-mu)^2/(2*sigma^2))
end

##apply a metropolis step using given hamiltonian H and given configuration

@everywhere function metropolis_step(r1,r2,H)
    p=exp(H(r1)-H(r2)) ##compare probability
    x=rand()

    ##make the step from r1 -> r2, or not
    if x<p
        
        return r2     
    else
        return r1
    end
end

##given analytic solution of the structure factor
function analytic_structure(x)
    b=x^2/6
    return 2/b^2*(exp(-b)-1+b)
end

##given long range solution of the structure factor
function long(x)
    b=x^2/(6)
    return 2/b
end

##given long range solution of the structure factor
function short(x)
    b=x^2/6
    return 1-b/3
end

###initialise a chain with N segments.
function make_chain(N=64)
    r=zeros(N,3)
    
    for i in 2:N
        x,y,z= 1/(sqrt(N-1))*(2*rand(3)-[1,1,1])   
        r2=[r[i-1,1]+x,r[i-1,2]+y,r[i-1,3]+z]
        r[i,:]=r2
    end                
    return r
end
    
    

###evolve the chain by attempting N metropolis steps per time step    
function evolve_rouse(r)
    N=length(r[:,1])
    H=Rouse_Hamiltonian
    for i in 1:N
        r1=copy(r)
        r2=displace_chain(r)
        r=metropolis_step(r1,r2,H)
    end                        
    return r 
end        

@everywhere function evolve_rouse_diblock(r)
    N=length(r[:,1])
    H=Rouse_diblock_Hamiltonian

    for i in 1:N
        r1=copy(r)
        r2=displace_chain(r)
        r=metropolis_step(r1,r2,H)
    end                        
    return r 
end


#computes the structure factor
function structure_factor(q,r)
    s=0
    N=length(r[:,1])
    
    for i in 1:N
        x,y,z=2*rand()-1,2*rand()-1,2*rand()-1   #generate a random vector q of length q     
        #x,y,z=1,1,1
        l=sqrt(3*(x^2+y^2+z^2))
        s+=exp.(1im*dot([q*x/l,q*y/l,q*z/l],r[i,:])) ##sum exponential
    end    
    return 1/N*abs(s)^2  ##return strucutre factor
end

#2.2
##compute the probability weight of the sliplink hamiltonian, i.e. exp(-H)
function weight_sliplink(r,links,A,beta=0.5)
    N=length(r[:,1])
    Z=length(links[:,1])
    sumN=0
    sumZ=0
    prod=1
    sumN=3/2 * (N - 1) * sum(norm.(r[2:end,:] - r[1:end-1,:]).^2)  #rouse contribution
    for i in 1:Z 
        if any(links.==0)
            print("Oh no")
        end
        sumZ+=3*(N-1)/2*beta*norm(r[links[i]]-A[i])^2    ##link attachement contibution
        for j in 1:Z
            if links[i]==links[j] && j!=i
                prod=0                           ## reject moving to occupied link
             end  
        end    
    end

    return exp(-sumN-sumZ)*prod   #return resulting probability weight
end

##displace random chain segment
function displace_chain(r,scaling=1)
    N=length(r[:,1])
    i =rand(1:N)
    l=sqrt(N-1)/scaling   #scaling introduced for debugging, generally not needed
    x,y,z=2*rand()-1,2*rand()-1,2*rand()-1
    r2=copy(r)
    r2[i,:]=[r[i,1]+x/l,r[i,2]+y/l,r[i,3]+z/l]    #return configuration with displaced bead  
    return r2
end
        
    

 #implementing tube renewal in case a link slips of the chain   
function tube_renewal(r,links2,A,beta=0.5)
    N=length(r[:,1])
    stdv=1/sqrt(3*(N-1)*beta)
    new_link=rand([1,N])
    links3=copy(links2)
    A2=copy(A)
       
    free_beads=collect(1:N)
    for o in links2
        deleteat!(free_beads, findall(x->x==o,free_beads))
        if new_link==o
          #  print(" ",'a'," ")
            new_link=N+1-new_link   #check for unoccupied chain end
        end
    end        
    
    
    ##make new attachement point a at chain end    
    new_a=[0.,0.,0.]   
        
    for j in 1:3
        new_a[j]=stdv*randn()+r[new_link,j]
    end 
    deleteat!(free_beads, findall(x->x==new_link,free_beads))   
    partner=rand(free_beads) #make new link 
    A2=vcat(A2,new_a')
    links3=vcat(links3,[new_link]')   

     ##make new attachement point a at new link 
    new_a=[0.,0.,0.]
        
    for j in 1:3
        new_a[j]=stdv*randn()+r[partner,j]
    end
        
    links3=vcat(links3,[partner]')
    A2=vcat(A2,new_a')      
    
    return A2,links3 ##return new attachement-link couple
end 

##full evolution step for sliplink model
function evolve_sliplink(r,links,A,beta=0.5)
    ## takes in polymer configuration r, list of indices of occupied links 
    ## and the respective attachement point a such that r[link[i]] is attached to a[i]

    N=length(r[:,1])
    delta_E=0         #this is for monitorin the change in energy as suggested

    for i in 1:N

        #attempt to move a bead
        H1=weight_sliplink(r,links,A)  
        Z=length(links) 
        r2=copy(r)
        r2=displace_chain(r2)
        
        H2=weight_sliplink(r2,links,A) 
        
 
        p=rand()
        ##attempt metropolis step according to weight H
        if p<H2/H1
            r=r2
            delta_E+= -log(H2/H1)  #monitoring energy
        end  

        #attempt to move a link along the chain
        links2=copy(links)
        A2=copy(A)
        j =rand(1:Z)  #chose a random link
        links2[j]+=rand([-1,1])     #move either left or right             
        
        #if it slips off the chain: 
        if links2[j]  == N+1 ||  links2[j]  == 0 
            k=j
            while k==j
                k=rand(1:Z) #remove random partner of slipped off link
            end
            #remove the link from list and from list of attachements as well
            if j>k
                A2= A2[1:end .!= j,:]
                links2=links2[1:end .!= j,:]
                A2= A2[1:end .!= k,:]
                links2=links2[1:end .!= k,:]
            else 
                A2= A2[1:end .!= k,:]
                links2=links2[1:end .!= k,:]
                A2= A2[1:end .!= j,:]
                links2=links2[1:end .!= j,:]
            end           
           
            A2,links2=tube_renewal(r,links2,A2)    #renew the tube, by attaching new link
           
        end
        H1=weight_sliplink(r,links,A) 
        H2=weight_sliplink(r,links2,A2)  #weigh the attempted link slip step, including tube renewal

        p=rand()
        #make the change
        if p<H2/H1
            links=links2
            A=A2
            delta_E+= -log(H2/H1)
        end    
    end
        
    return r,links,A,delta_E  
    ##return new configuration 
end      

#i dont think this function is used, however, it can plot the sliplink configuration, if you want a visual
function plot_sliplink(R,A_,links_,path="o")

    plot3d(R[:,1],R[:,2],R[:,3],label=:none)
    scatter!(A_[:,1],A_[:,2],A_[:,3],label=:none)

    for i in 1:length(links_)
        plot3d!([R[links_[i,1],1], A_[i,1] ],[R[links_[i],2], A_[i,2]] ,[R[links_[i],3], A_[i,3] ],linecolor="red",label=nothing, linestyle = :dash)
    end
    plot!(xlim=[-1,1],ylim=[-1,1],zlim=[-1,1])
    if path !="o"
        savefig(path)
    end    
end

## A-domain potential for diblock polymer evolution 
@everywhere function ω(r,w=10)  #adjust potential strength as needed
    if r[3]>0
        return w
    else 
        return  0
    end
end

#adjusted weight according to diblock hamiltonian
function weight_sliplink_diblock(r,links,A,beta=0.5)
    N=length(r[:,1])
    Z=length(links[:,1])
    sumN=0
    sumZ=0
    sumA=0
    sumB=0
    prod=1

    sumN=3/2 * (N - 1) * sum(norm.(r[2:end,:] - r[1:end-1,:]).^2) 

    for i in 1:Int(N/2)
        sumA+=1/N*ω(r[i,:])
        sumB+=1/N*(10-ω(r[i+Int(N/2),:]))   #new interface potential
    end

    for i in 1:Z 
        if any(links.==0)
            print("Oh no")
        end
        sumZ+=3*(N-1)/2*beta*norm(r[links[i]]-A[i])^2     #sliplink contribution
        for j in 1:Z
            if links[i]==links[j] && j!=i
                prod=0
             end  
        end    
    end

    return exp(-sumN-sumZ-sumA-sumB)*prod 
end

@everywhere function Rouse_diblock_Hamiltonian(r)
    N=length(r[:,1])
    sumN=0
    sumA=0
    sumB=0
    sumN=3/2 * (N - 1) * sum(norm.(r[2:end,:] - r[1:end-1,:]).^2)   #rouse cont  

    for i in 1:Int(N/2)
        sumA+=1/N*ω(r[i,:])
        sumB+=1/N*(10-ω(r[i+Int(N/2),:]))   #new interface potential
    end
    return sumA+sumB+sumN
end


#adjusted evolutionm for diblock sliplink, only difference is the weights 
function evolve_sliplink_diblock(r,links,A,beta=0.5)
  
    N=length(r[:,1])
    delta_E=0

    for i in 1:N
        
        H1=weight_sliplink(r,links,A)
        Z=length(links) 
        r2=copy(r)
        r2=displace_chain(r2)
        
        H2=weight_sliplink_diblock(r2,links,A) 
        
        #print(H2/H1," ")
        p=rand()
        
        if p<H2/H1
            r=r2
            delta_E+= -log(H2/H1) 
        end
        links2=copy(links)
        A2=copy(A)
        j =rand(1:Z) 
        links2[j]+=rand([-1,1])                 
        
        if links2[j]  == N+1 ||  links2[j]  == 0 
            k=j
            while k==j
                k=rand(1:Z) 
            end
           
            if j>k
                A2= A2[1:end .!= j,:]
                links2=links2[1:end .!= j,:]
                A2= A2[1:end .!= k,:]
                links2=links2[1:end .!= k,:]
            else 
                A2= A2[1:end .!= k,:]
                links2=links2[1:end .!= k,:]
                A2= A2[1:end .!= j,:]
                links2=links2[1:end .!= j,:]     ##all the same as before 
            end           
           
            A2,links2=tube_renewal(r,links2,A2)   
           
        end
        H1=weight_sliplink_diblock(r,links,A) 
        H2=weight_sliplink_diblock(r,links2,A2)

        p=rand()
 
        if p<H2/H1
            links=links2
            A=A2
            delta_E+= -log(H2/H1)
        end    
    end
        
    return r,links,A,delta_E  
end      
    
##initialise sliplink chain by returning the position vector r, the attached links, and their attachement points a
function make_chain_sliplink(N=64,beta=0.5,Z_=16)
    Z=Int(Z_/2)
    r=make_chain(N)       
    stdv=1/sqrt(3*(N-1)*beta)
    links=Array{Any}(undef,2*Z)
    A=zeros(2*Z,3)
    free_beads=collect(1:N)  #record the free link positions to avoid occupied positions
    for i in 1:Z     
        new_link=rand(free_beads)   #make a new link
        deleteat!(free_beads, findall(x->x==new_link,free_beads))

        new_a=[0.,0.,0.]  #add attachement point for the new link
        
        for j in 1:3
            new_a[j]=stdv*randn()+r[new_link,j]
        end
        A[2*i-1,:]=new_a
        ##again for a random partner link
        partner=rand(free_beads) 
        deleteat!(free_beads, findall(x->x==partner,free_beads))

        links[2*i-1,:]=[new_link]
        new_a=[0.,0.,0.]
        for j in 1:3
            new_a[j]=stdv*randn()+r[partner,j]
        end
        A[2*i,:]=new_a     
        links[2*i,:]=[partner]
    end 
    
    return r,links,A
end

##defining energy contributions to track energy evolution
function U(r)
    N=length(r[:,1])
    res=0
    for i in 2:N
        res+= 3*(N-1)/2*norm(r[i]-r[i-1])^2  #rouse contribution
    end
    return res
end

function W(r,links,A,beta=0.5)
    N=length(r[:,1])
    res=0
    for i in 1:length(links)
        res+= 3*(N-1)/2*beta*norm(r[links[i]]-A[i])^2   #sliplink contribution
    end
    return res
end

#compute MSD 
function averageG_sliplink(N=64,T=100,M=128) 
    g3=zeros(T)
    g1=zeros(T)
    Threads.@threads for i in 1:M
        R0,links_,A_=make_chain_sliplink()
        R=copy(R0)
        Rcm0=zeros(3)
            
        for o in 1:3    
            Rcm0[o]=1/N*sum(R0[:,o])  ##compute center of mass at t=0
        end    
                
        for j in 1:T
            Rcm=zeros(3)
            R,links_,A_,delatE=evolve_sliplink(R,links_,A_)
            
            for o in 1:3    
                Rcm[o]=1/N*sum(R[:,o])  
            end
            
            g3[j]+=1/M*norm(Rcm-Rcm0)^2  #record displacement of center of mass over time
            
            for o in 1:64
                g1[j]+=1/M*1/N*norm(R[o,:]-R0[o,:])^2  #record displacement of segments
            end 
        end
    end
return g1,g3
end

#same for rouse model
function averageG_rouse(N=64,T=100,M=128) 
    g3=zeros(T)
    g1=zeros(T)
    Threads.@threads for i in 1:M
        #print(i)
        R0=make_chain()
        R=copy(R0)
        Rcm0=zeros(3)
    
        for o in 1:3    
            Rcm0[o]=1/N*sum(R0[:,o])  
        end    
                
        for j in 1:T
            Rcm=zeros(3)
            R=evolve_rouse(R)
            
            for o in 1:3    
                Rcm[o]=1/N*sum(R[:,o])  
            end
            
            g3[j]+=1/M*norm(Rcm-Rcm0)^2
            
            for o in 1:N
                g1[j]+=1/M*1/N*norm(R[o,:]-R0[o,:])^2
            end 
        end
    end
return g1,g3
end


#same for sliplink diblock, really nothing new here
function averageG_sliplink_diblock(N=64,T=100,M=128,Z=16) 
    g3z=zeros(T)
    g1z=zeros(T)
    g3_=zeros(T)
    g1_=zeros(T)

    Threads.@threads for i in 1:M
        R0,links_,A_=make_chain_sliplink(N,0.5,Z)
        R=copy(R0)
        Rcm0=zeros(3)
            
        for o in 1:3    
            Rcm0[o]=1/N*sum(R0[:,o])  
        end    

        if Z!=0        
            for j in 1:T
                Rcm=zeros(3)
                R,links_,A_,delatE=evolve_sliplink_diblock(R,links_,A_)
                
                for o in 1:3    
                    Rcm[o]=1/N*sum(R[:,o])  
                end
                
                g3z[j]+=1/M*norm(Rcm[3]-Rcm0[3])^2
                g3_[j]+=1/M*norm(Rcm[1:2]-Rcm0[1:2])^2
                
                for o in 1:64
                    g1z[j]+=1/M*1/N*norm(R[o,3]-R0[o,3])^2
                    g1_[j]+=1/M*1/N*norm(R[o,1:2]-R0[o,1:2])^2
                end 
            end
        else
            for j in 1:T
                Rcm=zeros(3)
                R=evolve_rouse_diblock(R)
                
                for o in 1:3    
                    Rcm[o]=1/N*sum(R[:,o])  
                end
                
                g3z[j]+=1/M*norm(Rcm[3]-Rcm0[3])^2
                g3_[j]+=1/M*norm(Rcm[1:2]-Rcm0[1:2])^2
                
                for o in 1:64
                    g1z[j]+=1/M*1/N*norm(R[o,3]-R0[o,3])^2
                    g1_[j]+=1/M*1/N*norm(R[o,1:2]-R0[o,1:2])^2
                end 
            end

        end
    end
    return g1z,g3z,g1_,g3_
end

#the end