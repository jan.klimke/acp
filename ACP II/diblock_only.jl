include("ProjectII_lib.jl")
using Statistics
using LaTeXStrings
using Plots


####part 3 AB diblock polymer
##compute distance from interface for A and B seperately

##computing for rouse dynamics (Z=0)
T=10000
M=256
N=64
end2endA=[]
end2endB=[]
Threads.@threads for j in 1:M
    R=make_chain(N)
    for i in 1:T
        R=evolve_rouse_diblock(R)
    end
    append!(end2endA,R[1:Int(end/2),3])
    append!(end2endB,R[Int(end/2)+1:end,3])
end
histogram(end2endA,normalize=:pdf,bins=range(-2,2,30),label=:"A",alpha=0.6)
histogram!(end2endB,normalize=:pdf,bins=range(-2,2,30),label=:"B",alpha=0.6)  #histogram for A and B 
xlabel!("z")
ylabel!("counts (Z=0)")
savefig("ACP II/plots/diblockHistogramZ=0strongw.png")

##computing again for Z=N/4

end2endA=[]
end2endB=[]

Threads.@threads for j in 1:M
    R,links_,A_=make_chain_sliplink(N,0.5,16)
    for i in 1:T
        R,links_,A_,dE=evolve_sliplink_diblock(R,links_,A_)
    end
    append!(end2endA,R[1:Int(end/2),3])
    append!(end2endB,R[Int(end/2)+1:end,3])  ##evolve and record 
end
histogram(end2endA,normalize=:pdf,bins=range(-2,2,30),label=:"A",alpha=0.6)
histogram!(end2endB,normalize=:pdf,bins=range(-2,2,30),label=:"B",alpha=0.6)
xlabel!("z")
ylabel!("counts (Z=N/4)")
savefig("ACP II/plots/diblockHistogramZ=N4strongw.png")


###computing paralell g1z,g3z and perpendicular MSD g1_,g3_

 #first for Z=0
T=1000
g1z,g3z,g1_,g3_=averageG_sliplink_diblock(N,T,M,0)  

plot(g1z,label=:"g1")
plot!(g3z,label=:"g3")
plot!(yscale=:log10,xscale=:log10)
ylabel!("MSD z (Z=0)")
xlabel!("t")
savefig("ACP II/plots/MSD_sliplink_diblockzZ=0strongw.png")

plot(g1_,label=:"g1")
plot!(g3_,label=:"g3")
plot!(yscale=:log10,xscale=:log10)
ylabel!("MSD parallel (Z=0")
xlabel!("t")
savefig("ACP II/plots/MSD_sliplink_diblockparallelZ=0strongw.png")


###then for Z=N/4

T=1000
g1z,g3z,g1_,g3_=averageG_sliplink_diblock(N,T,M)  

plot(g1z,label=:"g1")
plot!(g3z,label=:"g3")
plot!(yscale=:log10,xscale=:log10)
ylabel!("MSD z (Z=N/4)")
xlabel!("t")
savefig("ACP II/plots/MSD_sliplink_diblockzstrongw.png")

plot(g1_,label=:"g1")
plot!(g3_,label=:"g3")
plot!(yscale=:log10,xscale=:log10)
ylabel!("MSD parallel (Z=N/4)")
xlabel!("t")
savefig("ACP II/plots/MSD_sliplink_diblockparallelstrongw.png")

##finished