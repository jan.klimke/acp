using Distributed

cip_pcs = ["c060", "c063", "c065", "c071", "c088", "c089", "c090", "c091", "c201", "c202", "c203", "c204", "c205", "c206", "c207", "c208", "c209"]
machines = [("$(pc).physikcip.uni-goettingen.de", :auto) for pc in cip_pcs]

if nworkers() <= 1
    println("Setting up remote workers...")
    addprocs(machines)
    println("Finished setting up remote workers!\n")
end
