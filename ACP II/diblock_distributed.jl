include("ProjectII_lib.jl")
include("cip.jl")
using Statistics
using LaTeXStrings


####part 3 AB diblock polymer
##compute distance from interface for A and B seperately

##computing for rouse dynamics (Z=0)
T=1000
M=256
N=64
end2endA=zeros(M)
end2endB=zeros(M)
R=zeros(M,N)
for i in 1:M
    R[i]=make_chain(N)
end
for i in 1:T
        pmap(evolve_rouse_diblock,R)
end

for i in 1:M
    end2endA[i]=R[i,1:Int(end/2),3]
    end2endB=R[i,Int(end/2)+1:end,3]
end

histogram(end2endA,normalize=:pdf,bins=range(-2,2,30),label=:"A",alpha=0.6)
histogram!(end2endB,normalize=:pdf,bins=range(-2,2,30),label=:"B",alpha=0.6)  #histogram for A and B 
xlabel!("z")
ylabel!("counts (Z=0)")
savefig("ACP II/plots/diblockHistogramZ=0long.png")

##computing again for Z=N/4

# end2endA=[]
# end2endB=[]

# for j in 1:M
#     R,links_,A_=make_chain_sliplink(N,0.5,16)
#     for i in 1:T
#         R,links_,A_,dE=evolve_sliplink_diblock(R,links_,A_)
#     end
#     append!(end2endA,R[1:Int(end/2),3])
#     append!(end2endB,R[Int(end/2)+1:end,3])  ##evolve and record 
# end
# histogram(end2endA,normalize=:pdf,bins=range(-2,2,30),label=:"A",alpha=0.6)
# histogram!(end2endB,normalize=:pdf,bins=range(-2,2,30),label=:"B",alpha=0.6)
# xlabel!("z")
# ylabel!("counts (Z=N/4)")
# savefig("ACP II/plots/diblockHistogramZ=N4long.png")
