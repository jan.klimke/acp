include("ProjectII_lib.jl")
using Plots
# end2end=zeros(1000)
# for i in 1:1000
#     R=make_chain(64)
#     end2end[i]=(R[1,1]-R[64,1])
# end

# Hist1=histogram(end2end ,normalize=:pdf,bins=range(-5,5,100)) 

# x=-5:0.1:5
# y=gauss.(x,1/sqrt(3))
# plot!(x,y)
# display(Hist1)

# R=make_chain(64)
# Polymer=plot3d(R[:,1],R[:,2],R[:,3],label=:none)
# display(Polymer)

R=make_chain(64)
Jiggle=@animate for i in 1:500
    global R=evolve_rouse(R)
    plot3d(R[:,1],R[:,2],R[:,3],label=:none)
    plot!(xlim=[-1,1],ylim=[-1,1],zlim=[-1,1])
end
gif(Jiggle,"jiggle.gif")


# M=256
# hist_endtoend=zeros(M,3)

# for i in 1:M
#     R=make_chain(64)
    
#     for j in 1:200
#         R=evolve_rouse(R)
#     end 
    
#     endtoend=(R[64,:]-R[1,:])
#     hist_endtoend[i,:]=endtoend
# end




# Hist2=histogram(hist_endtoend[:,1] ,normalize=:pdf,bins=range(-1,1,15))
# x=-1:0.02:1
# y=gauss.(x,1/3)
# plot!(x,y)
# display(Hist2)


# q=10 .^(range(-2,stop=2,length=30))
# S=zeros(length(q))

# for k in 1:length(q)
#     for i in 1:M
#         R=make_chain(128)
    
#         for j in 1:40
#             R=evolve_rouse(R)
#         end 
#         S[k]+=1/M*structure_factor(q[k],R)
#     end
# end

# Structure=plot(q,S/64,label=:"numerical")
# plot!(xscale=:log10,yscale=:log10)
# plot!(q,short.(q),label=:"short")
# plot!(q,analytic_structure.(q),label=:"analytical")
# plot!(q, long.(q),label=:"long",legend=:bottomleft,ylimits=(0.01,1))
# display(Structure)

# function evolve_sliplink_with_plot(T=20,beta=0.5)
#     R,links_,A_=make_chain_sliplink()
#     R0=copy(R)
#     U_=zeros(T+1)
#     W_=zeros(T+1)
#     E_=zeros(T+1)
#     Q=plot3d(R[:,1],R[:,2],R[:,3],label=:none)
#     scatter!(A_[:,1],A_[:,2],A_[:,3],label=:none)

#     for i in 1:length(links_)
#     plot3d!([R[links_[i,1],1], A_[i,1] ],[R[links_[i,1],2], A_[i,2]] ,[R[links_[i,1],3], A_[i,3] ],linecolor="red",label=nothing, linestyle = :dash)
#     end
#     plot!(xlim=[-1,1],ylim=[-1,1],zlim=[-1,1])
#     savefig(Q,"sliplink.png")
#     #display(Q)
#     U_[1]=U(R)
#     W_[1]=W(R,links_,A_)
#     for i in 2:T+1
#         R,links_,A_,dE=evolve_sliplink(R,links_,A_)
#         U_[i]=U(R)
#         W_[i]=W(R,links_,A_)
#         E_[i]=dE
#     end

#     Q2=plot3d(R[:,1],R[:,2],R[:,3],label=:none)
#     scatter!(A_[:,1],A_[:,2],A_[:,3],label=:none)

#     for i in 1:length(links_)
#         plot3d!([R[links_[i,1],1], A_[i,1] ],[R[links_[i],2], A_[i,2]] ,[R[links_[i],3], A_[i,3] ],linecolor="red",label=nothing, linestyle = :dash)
#     end
#     plot!(xlim=[-1,1],ylim=[-1,1],zlim=[-1,1])
#     savefig(Q2,"sliplinkevo.png")
#     display(U_[end]+W_[end])
#     display(U_[1]+W_[1]+sum(E_))
#     return U_,W_,E_
# end


# E=evolve_sliplink_with_plot(10)
# #display(E[1])
# Energies=plot([E[2][1]+E[1][1] + sum(E[3][1:i]) for i in 1:length(E[3])])
# plot!(E[2]+E[1])

# savefig(Energies,"energies.png")




#display(Q)

# global R_yöt,links_yöt,A_yöt=make_chain_sliplink()
# linkJiggle=@animate for j in 1:500
#      global R_yöt,links_yöt,A_yöt=evolve_sliplink(R_yöt,links_yöt,A_yöt)
#      plot3d(R_yöt[:,1],R_yöt[:,2],R_yöt[:,3],label=:none)
#      scatter!(A_yöt[:,1],A_yöt[:,2],A_yöt[:,3],label=:none)
#      plot!(xlim=[-1,1],ylim=[-1,1],zlim=[-1,1])
#      for i in 1:length(links_yöt[:,1])
#         plot3d!([R_yöt[links_yöt[i,1],1], A_yöt[i,1] ],[R_yöt[links_yöt[i,1],2], A_yöt[i,2]] ,[R_yöt[links_yöt[i,1],3], A_yöt[i,3] ],linecolor="red",label=nothing, linestyle = :dash)
#      end
# end
# gif(linkJiggle,"linkjiggle.gif")




# function averageG_sliplink(g1,g3,N=64,T=100,M=128) 

#     R0,links_,A_=make_chain_sliplink()
#     R=copy(R0)
#     Rcm0=zeros(3)
        
#     for o in 1:3    
#         Rcm0[o]=1/N*sum(R0[:,o])  
#     end    
            
#     for j in 1:T
#         Rcm=zeros(3)
#         R,links_,A_,delatE=evolve_sliplink(R,links_,A_)
        
#         for o in 1:3    
#             Rcm[o]=1/N*sum(R[:,o])  
#         end
        
#         g3[j]+=1/M*norm(Rcm-Rcm0)^2
        
#         for o in 1:64
#             g1[j]+=1/M*1/N*norm(R[o,:]-R0[o,:])^2
#         end 
#     end

# end
# T=100
# g3=zeros(T)
# g1=zeros(T)
# M=128
# for i in 1:M
#     averageG_sliplink(g1,g3)
#     print(i)
# end
# plot(g1,label=:"g1")
# plot!(g3,label=:"g3")
# plot!(yscale=:log10,xscale=:log10)
# savefig("MSD_sliplink.png")




# function averageG_sliplink_diblock(g1,g3,N=64,T=100,M=128) 

#     R0,links_,A_=make_chain_sliplink()
#     R=copy(R0)
#     Rcm0=zeros(3)
        
#     for o in 1:3    
#         Rcm0[o]=1/N*sum(R0[:,o])  
#     end    
            
#     for j in 1:T
#         Rcm=zeros(3)
#         R,links_,A_,delatE=evolve_sliplink_diblock(R,links_,A_)
        
#         for o in 1:3    
#             Rcm[o]=1/N*sum(R[:,o])  
#         end
        
#         g3[j]+=1/M*norm(Rcm-Rcm0)^2
        
#         for o in 1:64
#             g1[j]+=1/M*1/N*norm(R[o,:]-R0[o,:])^2
#         end 
#     end

# end
# T=100
# g3=zeros(T)
# g1=zeros(T)
# M=128
# for i in 1:M
#     averageG_sliplink_diblock(g1,g3)
#     print(i)
# end
# plot(g1,label=:"g1")
# plot!(g3,label=:"g3")
# plot!(yscale=:log10,xscale=:log10)
# savefig("MSD_sliplink_diblock.png")








# R,links_,A_=make_chain_sliplink()

# Q=plot3d(R[:,1],R[:,2],R[:,3],label=:none,linecolor="blue",)
# scatter!(A_[:,1],A_[:,2],A_[:,3],label=:none)
# for i in 1:length(links_)
#     plot3d!([R[links_[i,1],1], A_[i,1] ],[R[links_[i],2], A_[i,2]] ,[R[links_[i],3], A_[i,3] ],linecolor="red",label=nothing, linestyle = :dash)
# end
# plot!(xlim=[-1,1],ylim=[-1,1],zlim=[-1,1])
# savefig("sliplink.png")

# for i in 1:100
#     R,links_,A_,dE=evolve_sliplink(R,links_,A_)
# end

# Q=plot3d(R[:,1],R[:,2],R[:,3],label=:none,linecolor="blue",)
# scatter!(A_[:,1],A_[:,2],A_[:,3],label=:none)
# for i in 1:length(links_)
#     plot3d!([R[links_[i,1],1], A_[i,1] ],[R[links_[i],2], A_[i,2]] ,[R[links_[i],3], A_[i,3] ],linecolor="red",label=nothing, linestyle = :dash)
# end
# plot!(xlim=[-1,1],ylim=[-1,1],zlim=[-1,1])
# savefig("sliplinkevo.png")



R,links_,A_=make_chain_sliplink()
Jiggle=@animate for i in 1:500
    global R,links_,A_,dE=evolve_sliplink(R,links_,A_)
    plot3d(R[:,1],R[:,2],R[:,3],label=:none,linecolor="blue",)
    scatter!(A_[:,1],A_[:,2],A_[:,3],label=:none)
    for i in 1:length(links_)
        plot3d!([R[links_[i,1],1], A_[i,1] ],[R[links_[i],2], A_[i,2]] ,[R[links_[i],3], A_[i,3] ],linecolor="red",label=nothing, linestyle = :dash)
    end
    plot!(xlim=[-1,1],ylim=[-1,1],zlim=[-1,1])
end
gif(Jiggle,"linkjiggle.gif")