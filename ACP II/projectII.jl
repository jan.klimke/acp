include("ProjectII_lib.jl")
using Statistics
using LaTeXStrings
using Plots

#endto end histogram rouse evolution

M=256
N=64
end2end=zeros(M,3)

Threads.@threads for i in 1:M
    R=make_chain(64)                ##initialise a chain
    for j in 1:400
        R=evolve_rouse(R)           #evolve the chain for 400 steps
    end
    end2end[i,:]=(R[1,:]-R[64,:])       #compute R
end
histogram(end2end[:,1] ,normalize=:pdf,bins=range(-2,2,30),label=:"Rx")     #plot the x or [1] component of R
Var=sqrt(mean([norm(end2end[i,:])^2 for i in 1:M]))      #compute the variance for comparison to expected result of 1 
print(Var)
x=-2:0.03:2
y=gauss.(x,1/sqrt(3))      #plot a gaussian
plot!(x,y,label=:"gaussian")
xlabel!("R")
ylabel!("counts")
savefig("ACP II/plots/end2endHistogramRouse.png")  #save the figure


###structure factor computation
T=1000
q=10 .^(range(-2,stop=2,length=40))
S=zeros(length(q))

for k in 1:length(q)
    Threads.@threads for i in 1:M
        R=make_chain(64)
    
        for j in 1:T              #initialise and evolve the chain
            R=evolve_rouse(R)
        end 
        S[k]+=1/M*structure_factor(q[k],R)    #average S over M indepentend runs    
    end
end
##plot the result and compare to analytic solution
plot(q,S/64,label=:"numerical")
plot!(xscale=:log10,yscale=:log10)
plot!(q,short.(q),label=:"short")
plot!(q,analytic_structure.(q),label=:"analytical")
plot!(q, long.(q),label=:"long",legend=:bottomleft,ylimits=(0.01,1))
xlabel!("|q|")
ylabel!("S(q)")
savefig("structurefactor.png")


##compute MSD
T=1000
g1,g3=averageG_rouse(64,T,M)

plot(g1,label=:"g1")
plot!(g3,label=:"g3")
plot!(yscale=:log10,xscale=:log10)
ylabel!("MSD")
xlabel!("t")
savefig("ACP II/plots/MSD_rouse.png")

G=[g3[i]/(6*i) for i in 1:length(g3)]
plot(G,label=:"g3(t)/6t")
plot!([[1,length(G)]],[G[end],G[end]],label=:"D")
ylabel!("Diffusion constant")
xlabel!("t")
savefig("ACP II/plots/Diffusion_rouse.png")


###part 2 Sliplink
##end to end histogram sliplink evolution

M=256
N=64
end2end=zeros(M,3)
Threads.@threads for i in 1:M
    R,links_,A_=make_chain_sliplink(N)
    for j in 1:T
        R,links_,A_=evolve_sliplink(R,links_,A_)    #same as before, initialise and evolve chain
    end
    end2end[i,:]=(R[1,:]-R[64,:])   #record end-to-end vector
end
Hist1=histogram(end2end[:,1] ,normalize=:pdf,bins=range(-2,2,30),label=:"Rx") 
Var=sqrt(mean([norm(end2end[i,:])^2 for i in 1:M]))
print(Var)
x=-2:0.03:2
y=gauss.(x,1/sqrt(3))   #plot x component of R and plot gaussian
plot!(x,y,label=:"gaussian")
xlabel!("R")
ylabel!("counts")
savefig("ACP II/plots/end2endHistogramsliplink.png")


##compute MSD for the sliplink model
T=1000
g1,g3=averageG_sliplink(64,T,M)

plot(g1,label=:"g1")
plot!(g3,label=:"g3")
plot!(yscale=:log10,xscale=:log10)
ylabel!("MSD")
xlabel!("t")
savefig("ACP II/plots/MSD_sliplink.png")

G=[g3[i]/(6*i) for i in 1:length(g3)]
plot(G,label=:"g3(t)/6t")
plot!([[1,length(G)]],[G[end],G[end]],label=:"D")
ylabel!("Diffusion constant")
xlabel!("t")
savefig("ACP II/plots/Diffusion_sliplink.png")

####part 3 AB diblock polymer
##compute distance from interface for A and B seperately

##computing for rouse dynamics (Z=0)
T=10000
M=256
N=64
end2endA=[]
end2endB=[]
Threads.@threads for j in 1:M
    R=make_chain(N)
    for i in 1:T
        R=evolve_rouse_diblock(R)
    end
    append!(end2endA,R[1:Int(end/2),3])
    append!(end2endB,R[Int(end/2)+1:end,3])
end
histogram(end2endA,normalize=:pdf,bins=range(-2,2,30),label=:"A",alpha=0.6)
histogram!(end2endB,normalize=:pdf,bins=range(-2,2,30),label=:"B",alpha=0.6)  #histogram for A and B 
xlabel!("z")
ylabel!("counts (Z=0)")
savefig("ACP II/plots/diblockHistogramZ=0.png")

##computing again for Z=N/4, this takes ages so feel free to reduce T or M to speed up the process

end2endA=[]
end2endB=[]

Threads.@threads for j in 1:M
    R,links_,A_=make_chain_sliplink(N,0.5,16)
    for i in 1:T
        R,links_,A_,dE=evolve_sliplink_diblock(R,links_,A_)
    end
    append!(end2endA,R[1:Int(end/2),3])
    append!(end2endB,R[Int(end/2)+1:end,3])  ##evolve and record 
end
histogram(end2endA,normalize=:pdf,bins=range(-2,2,30),label=:"A",alpha=0.6)
histogram!(end2endB,normalize=:pdf,bins=range(-2,2,30),label=:"B",alpha=0.6)
xlabel!("z")
ylabel!("counts (Z=N/4)")
savefig("ACP II/plots/diblockHistogramZ=N4.png")


###computing paralell g1z,g3z and perpendicular MSD g1_,g3_

 #first for Z=0
T=1000
g1z,g3z,g1_,g3_=averageG_sliplink_diblock(N,T,M,0)  

plot(g1z,label=:"g1")
plot!(g3z,label=:"g3")
plot!(yscale=:log10,xscale=:log10)
ylabel!("MSD z (Z=0)")
xlabel!("t")
savefig("ACP II/plots/MSD_sliplink_diblockzZ=0.png")

plot(g1_,label=:"g1")
plot!(g3_,label=:"g3")
plot!(yscale=:log10,xscale=:log10)
ylabel!("MSD parallel (Z=0)")
xlabel!("t")
savefig("ACP II/plots/MSD_sliplink_diblockparallelZ=0.png")


###then for Z=N/4

T=1000
g1z,g3z,g1_,g3_=averageG_sliplink_diblock(N,T,M)  

plot(g1z,label=:"g1")
plot!(g3z,label=:"g3")
plot!(yscale=:log10,xscale=:log10)
ylabel!("MSD z (Z=N/4)")
xlabel!("t")
savefig("ACP II/plots/MSD_sliplink_diblockz.png")

plot(g1_,label=:"g1")
plot!(g3_,label=:"g3")
plot!(yscale=:log10,xscale=:log10)
ylabel!("MSD parallel (Z=N/4)")
xlabel!("t")
savefig("ACP II/plots/MSD_sliplink_diblockparallel.png")

##finished