include("project3_lib.jl")    #get the _lib file
E=diagonalised_Heisenberg(8,3)
 
vline(E,color="red",linewidth=0.3,label="")
savefig("ACP III/plots/spectrum_Heisenberg_test.png")  #test the hamiltonian just for fun, this isnt really necessary 


#define disorder parameters and chain lengths
W=[0.5,3,8]               
L=[8,10,12,14]

#number of independent disorder realisations
N_r=100   

open("ACP III/plots/gap_ratio.txt", "w") do file    #open text file to save results in 

    for j in L
        for i in W

            A=[]
            display(j)                  #helps keep track of where we are in the run

            for k in 1:N_r
                push!(A,gap_ratio(j,i))     #compute and append gap-ratio
            end

            A=collect(Iterators.flatten(A))
            write(file, "L=$j,W=$i \n")
            write(file, "$A\n")                   #save measured gap-ratios r in file 
            histogram(A,normalize=:pdf,bins=30,label="L=$j,W=$i")    #plot distribution of r
            xlabel!("r")
            ylabel!("counts")
            savefig("ACP III/plots/Histogram_gap_ratio_W=$(i)_L=$j.png")

        end
    end
end

#measure average [r] against disorder strength W, one could also do this simultaneos to the first part,
# but in case you want a better resolution in W i did it again here, so double the computation time


L=[8,10,12,14]
dots=[100,100,20,5]    #vary the number of evaluations with L
plot()

open("ACP III/plots/gap_ratio_average.txt", "w") do file   #save results in new file
   

    for j in 1:length(L)
        W=LinRange(0.5,12,dots[j])
        E=zeros(length(W))
        display(L[j])
        for i in 1:length(W)
            
            A=[]

            for k in 1:N_r
                push!(A,gap_ratio(L[j],W[i]))  #same as before, compute the gap ratio N_r times 
            end

            A=collect(Iterators.flatten(A))
            E[i]=mean(A)            #get the average of the independent runs
            
            

        end
        plot!(W,E,label="L=$(L[j])")    #plot for each L
        
        write(file, "L=$(L[j]) \n")
        write(file, "$(E)\n")   #save
    end  
end 
ylabel!("average gap ratio [r]")
xlabel!("disorder W")
savefig("ACP III/plots/mean_gap_L_all.png")    


#distribution of magnetisation/average spin
W=[0.5,3,4,8,12]
L=[8,10,12,14]

N_r=100

open("ACP III/plots/mean_Sz.txt", "w") do file    

    for j in L
        for i in W

            A=[]
            display(j)

            for k in 1:N_r
                push!(A,Zspin(j,i))         #compute average spin 
            end

            A=collect(Iterators.flatten(A))
            write(file, "L=$j,W=$i \n")
            write(file, "$A\n")
            histogram(A,normalize=:pdf,label="L=$j,W=$i")    #histogram of spins
            xlabel!("S_z")
            ylabel!("counts")
            savefig("ACP III/plots/Histogram_S_z_W=$(i)_L=$j.png")

        end
    end
end

#done. A whole run can easily take 4h, or even more if you want more accuracy. I would reccomend not doing L=14 and L=12 too often, especially L=14 takes ages,
# so if you want a faster result, cut down there 

