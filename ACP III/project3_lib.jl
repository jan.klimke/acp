using Statistics
using LaTeXStrings
using Plots
using LinearAlgebra
using BitOperations



#construct computational basis for spins as bits  
function construct_basis(spins)
    basis = Vector{BitVector}(undef, 0)
    N = 2^spins

    for i in 0:N-1
        vec = BitVector(digits(
            i,
            base = 2,
            pad = spins
        ))

        push!(basis, vec)
    end

    return basis
end

#construct the Heisenberg hamiltonian as a matrix 
function Hamiltonian(N,W=1,J=1)
    H = zeros(2^N,2^N)
    ϵ=[W*(rand()-1/2) for i in 1:N]
    for a in 0:2^N-1
        for i in 0:N-1
            j = mod(i+1,N)      #periodic boundary
            H[a+1,a+1] += ϵ[i+1]*(bget(a,i)-1/2) ##diagonal contribution by random lattice potential
            if bget(a,i) == bget(a,j)
                H[a+1,a+1] += J/4         ##diagonal contribution S_z*S_z (reminder that S=+-1/2, so Sz^2=1/4)
            else
                H[a+1,a+1] += -J/4
                b = bflip(a, i)          #off-diagonal terms/ nearest neighbour interaction 
                b = bflip(b, j)
                H[a+1, b+1] += J/2
            end 
        end
    end

    return H
end

#measure magnetisation ∑ S_z of given (pure) spin-vector
function m_z(state::BitVector)
    res = 0

    for bit in state
        res += bit - 1/2
    end

    return res
end

#measure magnetisation ∑ S_z of given mixed state

function m_z(vector::Vector{Float64})
    basis = construct_basis(Int(log2(length(vector))))      # ToDo: check if length 2^x

    return sum(vector .* m_z.(basis))
end


#give base states of the same magnetisation m 

function block_basis(spins, m)
    basis = construct_basis(spins)

    bbasis = Vector{BitVector}(undef, 0)
    btransform = Vector{Integer}(undef, 0)

    for i in 1:2^spins
        if m_z(basis[i]) == m
            push!(bbasis, basis[i])
            push!(btransform, i)
        end
    end

    return bbasis, btransform
end

#block diagonalisation algorithm
function diagonalised_Heisenberg(N,W=1,t=1)
    H=Hamiltonian(N,W,t) #full hamiltonian (non-diagonal)
    #display(H)
    Energies=[]
    for m in -N/2:N/2
        _, btransform = block_basis(N, m)

        BlockH = H[btransform, btransform]  #transform Hamilton into block with magnetisation m
        #display(BlockH)
        V=eigen(BlockH)    #diagonalise the much smaller blocks 
        #display(V)
        append!(Energies,V.values)  #gather the soectrum 
    end

    return Energies   #return full diagonal of the hamiltonian 
end

function r(E)
    res = zeros(length(E) - 1)
    δ = E[2:end] - E[1:end-1]

    # r₁ is not defined
    res[1] = NaN

    # calculate the gap ratios component-wise
    res[2:end] = @. min(δ[1:end-1], δ[2:end]) / max(δ[1:end-1], δ[2:end])
end


#compute gap-ratio 
function gap_ratio(N,W=1,t=1)
    H=Hamiltonian(N,W,t)
    m=0                       #only m=0
    _, btransform = block_basis(N, m)
    BlockH = H[btransform, btransform]
    V=eigen(BlockH)    #get spectrum
    #display(V.values)
    B=length(V.values)
    E=V.values
    E_min = minimum(E)
    E_max = maximum(E)

    ϵ= (E .- E_min) / (E_max - E_min)  
    index= sortperm(@. abs(ϵ- 1/2))  #compute |energy density -1/2|

    gap_ratios= r(E)[sort(index[1:6])[2:5]]
    return gap_ratios
end



#compute average z-spin, similar to gap ratio 
function Zspin(N,W=1,t=1)
    H=Hamiltonian(N,W,t)
    #display(H)
    Energies=[]
    S=[]
    result=zeros(6)
    for m in -N/2:N/2                           #for all m/<S_z> this time 
        _, btransform = block_basis(N, m)

        BlockH = H[btransform, btransform]
        #display(BlockH)
        V=eigen(BlockH)
        #display(V)
        append!(Energies,V.values)            #save energies
        for i in V.values
            append!(S,m)                      #save respective spin
        end
    end
    E=Energies
    E_min = minimum(E)
    E_max = maximum(E)

    ϵ= (E .- E_min) / (E_max - E_min)  
    index = sortperm(@. abs(ϵ- 1/2))  #compute |energy density -1/2|

    result= S[sort(index[1:6])]/N
    return result
end


Zspin(8,1)
